// Khai báo đầu vào
var input = [[3, 5], 9]

function kienTra(input) {
    let test = false
    input.forEach(element => {
        if (!Array.isArray(element)) {
            test = true
        }
    })
    return test
}

// Khai báo hàm
function timDiemChung(input) {
    // Khai báo các biến
    let tongSoPhanTuMangLon
    let mangTrungLap
    let mangTamThoi = []
    // Tính tổng số phần tử trong mảng lớn
    tongSoPhanTuMangLon = input.length
    // Kiểm tra mảng có rỗng hay không
    if (tongSoPhanTuMangLon == 0) {
        // Nếu đúng thì trả ra kết quả "Mảng lớn rỗng"
        return 'Mảng lớn rỗng'
    }
    // Nếu sai thì kiểm tra từng phần tử trong mảng lớn có phải 1 mảng hay không
    else if (kienTra(input)) {
        // Nếu có 1 phần tử không phải mảng thì trả ra kết quả "Mảng lớn không hợp lệ"
        return 'Mảng lớn không hợp lệ'
    }
    // Nếu không thì kiểm tra số phần tử trong mảng lớn có bằng 1 hay không
    else if (tongSoPhanTuMangLon == 1) {
        // Nếu có thì trả ra kết quả là toàn bộ mảng con
        return `Mảng có điểm chung: ${input[0]}`
    } else {
        // Nếu sai thì lọc các phần tử trùng lặp trong mảng đầu tiên và gán vào biến mảng trùng lặp
        mangTrungLap = [... new Set(input[0])]
        // Xét lần lượt từng giá trị trong mảng lớn, bắt đầu từ mảng số 2
        for (let i = 1; i < tongSoPhanTuMangLon; i++) {
            // Xét mảng tạm thời bằng 1 mảng rỗng
            mangTamThoi = []
            // Xét lần lượt từng vị trí của mảng trùng lặp
            mangTrungLap.forEach(viTriXet => {
                // Xét lần lượt từng vị trí của mảng con đang xét
                let j = 0
                while (j < input[i].length) {
                    // So sánh xem vị trí xét có bằng vị trí so sánh hay không
                    if (viTriXet === input[i][j]) {
                        // Nếu đúng thì thêm giá trị đó vào mảng tạm thời và dừng vòng lặp của mảng con đang xét
                        mangTamThoi.push(viTriXet)
                        break
                    }
                    j++
                }
            })
            // Xét mảng trùng lặp bằng mảng tạm thời
            mangTrungLap = mangTamThoi
        }
        // Trả ra kết quả cuối cùng là các phần tử trong mảng trùng lặp
        return `Mảng có điểm chung: ${mangTrungLap}`
    }
}

// Gọi hàm để sử dụng
console.log(timDiemChung(input))