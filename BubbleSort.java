package com.example.testbublesort;

import java.util.ArrayList;
import java.util.List;

import java.io.IOException;
// Tạo ra 1 đối tượng kết quả trả về
class ketQua{
    // Khai báo thuộc tính cho đối tượng
    public boolean status;
    public List<Integer> content;
    public String message;

    // Xác định kiểu trả về của đối tượng
    public String toString() {
        return "output [\nstatus=" + status + ",\ncontent=" + content + ",\nmessage=" + message + "\n]";
    }
}

public class HelloApplication {
    public static ketQua sapXep(List<Integer> daySo) {
        // Khởi tạo đối tượng trả về
        ketQua output= new ketQua();
        // Khai báo ra biến tổng số lượng phần tử
        int tongSoLuongPhanTu;
        // Khai báo ra biến số lần không đổi
        int soLanKhongDoi;
        // Tính tổng số phần tử trong dãy
        tongSoLuongPhanTu = daySo.size();
        // Kiểm tra dãy có phần tử nào không?
        if (tongSoLuongPhanTu == 0) {
            // Nếu đúng, thực hiện cập nhật đối tượng kết quả trả ra
            output.status = false;
            output.content = new ArrayList<Integer>();
            output.message = "Dãy đã cho không có phần tử nào";
        }
        // Nếu sai, kiểm tra tổng số lượng phần tử có bằng 1 hay không?
        else if (tongSoLuongPhanTu == 1) {
            // Nếu đúng, thực hiện cập nhật đối tượng kết quả trả ra
            output.status = true;
            output.content = daySo;
            output.message = "Dãy đã cho chỉ có 1 phần tử";
        }
        // Nếu sai, tạo vòng lặp do-while để thể hiện các bước xử lí là so sánh kiểm tra từ đầu đến cuối dãy
        else {
            do {
                // Gọi ra và để biến số lần không đổi có giá trị bằng 0
                soLanKhongDoi = 0;
                // Gọi ra lần lượt các phần tử có trong dãy làm phần tử đang xét
                for(int i = 0; i <= tongSoLuongPhanTu - 2; i++){
                    // Xác định được vị trí đang so sánh
                    int j = i+1;
                    // Kiểm tra giá trị của vị trí đang xét với vị trí đang so sánh
                    if(daySo.get(i) > daySo.get(j)){
                        // Nếu đúng, tạo ra biến giaTriBanDau và đổi giá trị của 2 vị trí
                        int temp = daySo.get(i);
                        daySo.set(i, daySo.get(j));
                        daySo.set(j, temp);
                    }else{
                        // Nếu sai, không thực hiện đổi giá trị và +1 vào số lần không đổi
                        soLanKhongDoi++;
                    }
                }
            } while(soLanKhongDoi < tongSoLuongPhanTu -1);
            // Cập nhật đối tượng trả ra kết quả
            output.status = true;
            output.content = daySo;
            output.message = "Dãy đã được sắp xếp";
        }
        // Trả về đối tượng
        return output;
    }
    //Khai báo hàm chính trong lớp
    public static void main(String[] args) {
        //Khởi tạo một danh sách chứa các phần tử cần sắp xếp
        List<Integer> arr = new ArrayList<Integer>();
        arr.add(2);
        arr.add(5);
        arr.add(3);
        arr.add(8);
        arr.add(1);
        arr.add(9);
        arr.add(0);
        arr.add(7);
        arr.add(6);
        //Gọi sắp xếp và in ra kết quả
        System.out.println(sapXep(arr));
    }
}